<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Define hasMany relation with BankAccount model.
     *
     * @return void
     */
    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class);
    }
}
