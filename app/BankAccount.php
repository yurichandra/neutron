<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'account_number',
    ];

    /**
     * Define belongsTo relation with Bank model.
     *
     * @return void
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
