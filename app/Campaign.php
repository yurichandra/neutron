<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'student_id',
        'title',
        'description',
        'date',
        'donation_target',
        'donation_received',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'donation_target' => 'double',
        'donation_received' => 'double',
    ];

    /**
     * Define relation morphMany with Photo model.
     *
     * @return void
     */
    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }

    /**
     * Define relation hasOne with Student model.
     *
     * @return void
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Define relation belongsTo with User model.
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Define hasMany relation with Donation model.
     *
     * @return void
     */
    public function donations()
    {
        return $this->hasMany(Donation::class);
    }

    /**
     * Define hasMany relation with reports.
     *
     * @return void
     */
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}
