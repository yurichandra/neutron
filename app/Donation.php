<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'user_id',
        'donation_status_id',
        'amount',
        'occured_at',
        'verify_at',
        'meta',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'double',
    ];

    /**
     * Define belongsTo relation against DonationStatus model.
     *
     * @return void
     */
    public function status()
    {
        return $this->belongsTo(DonationStatus::class, 'donation_status_id');
    }

    /**
     * Define belongsTo relation against User model.
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Define belongsTo relation against Campaign model.
     *
     * @return void
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
}
