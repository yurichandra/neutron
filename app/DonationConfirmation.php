<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationConfirmation extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'donation_id',
        'bank_id',
        'account_holder',
        'amount_to_paid',
        'date',
    ];

    /**
     * Casted attribute
     *
     * @var array
     */
    protected $casts = [
        'amount_to_paid' => 'double',
    ];

    /**
     * Define belongsTo relation with Donation model.
     *
     * @return void
     */
    public function donation()
    {
        return $this->belongsTo(Donation::class);
    }
}
