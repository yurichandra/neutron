<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationStatus extends Model
{
    const PENDING = 1;

    const WAITING_TO_VERIFY = 2;

    const COMPLETED = 3;

    const WAITING_FOR_PAYMENT = 4;

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Define hasMany relation with donation model.
     *
     * @return void
     */
    public function donations()
    {
        return $this->hasMany(Donation::class);
    }
}
