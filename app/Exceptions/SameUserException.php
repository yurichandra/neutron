<?php

namespace App\Exceptions;

class SameUserException extends \Exception
{
    protected $message = 'user_can`t_be_same';
}
