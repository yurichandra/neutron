<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * FIllable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'fileable_id',
        'fileable_type',
        'type',
        'name',
    ];

    /**
     * Get owning of fileable model.
     *
     * @return void
     */
    public function fileable()
    {
        return $this->morphTo();
    }

    /**
     * Accessor of url attribute.
     *
     * @return void
     */
    public function getUrlAttribute()
    {
        return env('AWS_BUCKET_URL') . "/{$this->name}";
    }
}
