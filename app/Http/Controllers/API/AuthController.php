<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class AuthController extends RestController
{
    protected $transformer = UserTransformer::class;

    /**
     * Handle whoami.
     *
     * @param Request $request
     * @return void
     */
    public function whoami(Request $request)
    {
        try {
            $user = $request->user();

            return $this->response($this->generateItem($user));
        } catch (\Exception $e) {
            return $this->sendUnauthorizedResponse('Unauthorized');
        }
    }
}
