<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\BankAccountService;
use App\Transformers\BankAccountTransformer;

class BankAccountController extends RestController
{
    protected $transformer = BankAccountTransformer::class;

    /**
     * Handle request to fetch all bank accounts by bank,
     *
     * @param BankAccountService $bank_account_service
     * @param int $bank_id
     * @return void
     */
    public function getByBank(BankAccountService $bank_account_service, $bank_id)
    {
        try {
            $bank_accounts = $bank_account_service->getByBank($bank_id);

            return $this->response($this->generateCollection($bank_accounts));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
