<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\BankService;
use App\Transformers\BankTransformer;

class BankController extends RestController
{
    protected $transformer = BankTransformer::class;

    /**
     * Handle request to fetch all banks.
     *
     * @param BankService $bank_service
     * @return void
     */
    public function get(BankService $bank_service)
    {
        try {
            $bank = $bank_service->get();

            return $this->response($this->generateCollection($bank));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
