<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\CampaignService;
use App\Transformers\CampaignTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CampaignController extends RestController
{
    protected $transformer = CampaignTransformer::class;

    /**
     * @var CampaignService
     */
    protected $campaign_service;

    /**
     * CampaignController construct.
     *
     * @param CampaignService $campaign_service
     */
    public function __construct(CampaignService $campaign_service)
    {
        parent::__construct();

        $this->campaign_service = $campaign_service;
    }

    /**
     * Handle request to fetch all campaigns.
     *
     * @return void
     */
    public function get()
    {
        try {
            $campaigns = $this->campaign_service->get();

            return $this->response($this->generateCollection($campaigns));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Handle request to fetch single campaign.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $campaign = $this->campaign_service->find($id);

            return $this->response($this->generateItem($campaign));
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Handle request to fetch all campaign by user.
     *
     * @return void
     */
    public function getMyCampaign()
    {
        try {
            $user_id = Auth::user()->id;
            $campaigns = $this->campaign_service->findByUser($user_id);

            return $this->response($this->generateCollection($campaigns));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Handle request to store new campaign.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
            'donationTarget' => 'required',
            'studentId' => 'required',
            'photoId' => 'required',
        ]);

        try {
            $data = [
                'title' => $request->title,
                'description' => $request->description,
                'date' => $request->date,
                'donation_target' => $request->donationTarget,
                'student_id' => $request->studentId,
                'user_id' => Auth::user()->id,
                'donation_received' => 0,
                'photoIds' => [ $request->photoId ],
            ];

            $campaign = $this->campaign_service->create($data);

            return $this->response($this->generateItem($campaign), 201);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
