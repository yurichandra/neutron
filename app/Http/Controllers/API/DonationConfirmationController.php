<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\DonationConfirmationService;
use App\Transformers\DonationConfirmationTransformer;

class DonationConfirmationController extends RestController
{
    protected $transformer = DonationConfirmationTransformer::class;

    /**
     * Handler to fetch all confirmation of donation.
     *
     * @param DonationConfirmationService $donation_confirmation_service
     * @return void
     */
    public function get(DonationConfirmationService $donation_confirmation_service)
    {
        try {
            $donations = collect($donation_confirmation_service->get())
                ->filter(function ($item) {
                    return $item->donation->status->id !== 3;
                });

            return $this->response($this->generateCollection($donations));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
