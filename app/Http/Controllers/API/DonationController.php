<?php

namespace App\Http\Controllers\API;

use App\DonationStatus;
use App\Events\DonationVerified;
use App\Http\Controllers\RestController;
use App\Services\DonationConfirmationService;
use App\Services\DonationService;
use App\Services\UniqueCodeService;
use App\Transformers\DonationTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DonationController extends RestController
{
    protected $transformer = DonationTransformer::class;

    /**
     * @var DonationService
     */
    protected $donation_service;

    /**
     * @var DonationConfirmationService
     */
    protected $donation_confirmation_service;

    protected $unique_code_service;

    /**
     * DonationController construct.
     *
     * @param DonationService $donation_service
     */
    public function __construct(
        DonationService $donation_service,
        DonationConfirmationService $donation_confirmation_service,
        UniqueCodeService $unique_code_service
    ) {
        parent::__construct();

        $this->donation_service = $donation_service;
        $this->donation_confirmation_service = $donation_confirmation_service;
        $this->unique_code_service = $unique_code_service;
    }

    /**
     * Handler to fetch all donation.
     *
     * @return void
     */
    public function get()
    {
        try {
            $donations = $this->donation_service->get();

            return $this->response($this->generateCollection($donations));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Find a donation.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $donation = $this->donation_service->find($id);

            return $this->response($this->generateItem($donation));
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Return all donation by current user.
     *
     * @return void
     */
    public function getMyDonation()
    {
        try {
            $user_id = Auth::user()->id;
            $donations = $this->donation_service->getMyDonation($user_id);

            return $this->response($this->generateCollection($donations));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Store a new donation.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $request->validate([
            'campaignId' => 'required',
            'amount' => 'required',
            'bankAccountId' => 'required',
        ]);

        try {
            $meta = json_encode([
                'bank_account_id' => $request->bankAccountId,
                'unique_code' => $this->unique_code_service->generate(),
            ]);

            $data = [
                'campaign_id' => $request->campaignId,
                'amount' => $request->amount,
                'meta' => $meta,
                'donation_status_id' => DonationStatus::WAITING_FOR_PAYMENT,
                'occured_at' => Carbon::now(),
                'user_id' => Auth::user()->id,
            ];

            $donation = $this->donation_service->create($data);

            return $this->response($this->generateItem($donation), 201);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Confirm donation
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function confirm(Request $request, $id)
    {
        $request->validate([
            'accountHolder' => 'required',
            'amount' => 'required',
            'date' => 'required',
        ]);

        try {
            $data = [
                'account_holder' => $request->accountHolder,
                'amount' => $request->amount,
                'date' => $request->date,
            ];

            $this->donation_service->confirm($id, $data);

            return response()->json();
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Verify payment
     *
     * @param int $id
     * @return void
     */
    public function verify($id)
    {
        try {
            $donation = $this->donation_confirmation_service->find($id)->donation;
            event(new DonationVerified($donation));

            return response()->json();
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
