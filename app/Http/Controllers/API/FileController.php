<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\FileService;
use App\Transformers\FileTransformer;
use Illuminate\Http\Request;

class FileController extends RestController
{
    protected $transformer = FileTransformer::class;

    /**
     * Handler to store new file.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request, FileService $file_service)
    {
        $request->validate([
            'file' => 'required',
        ]);

        try {
            $file = $request->file;
            $data = [
                'file' => $file,
                'name' => $file->hashName(),
                'type' => $file->getMimeType(),
            ];

            $file = $file_service->create($data);

            return $this->response($this->generateItem($file), 201);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
