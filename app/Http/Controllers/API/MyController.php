<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class MyController extends RestController
{
    public function getMyActivity(UserService $user_service)
    {
        try {
            $user_id = Auth::user()->id;
            $activites = $user_service->myActivity($user_id);

            return response()->json($activites);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
