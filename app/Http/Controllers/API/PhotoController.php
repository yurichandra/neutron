<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Transformers\PhotoTransformer;
use App\Services\PhotoService;
use Illuminate\Http\Request;

class PhotoController extends RestController
{
    protected $transformer = PhotoTransformer::class;

    /**
     * @var PhotoService
     */
    protected $photo_service;

    /**
     * PhotoController construct.
     *
     * @param PhotoService $photo_service
     */
    public function __construct(PhotoService $photo_service)
    {
        parent::__construct();

        $this->photo_service = $photo_service;
    }

    /**
     * Handler to store new image.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required',
        ]);

        try {
            $photo = $request->photo;
            $data = [
                'path' =>$photo->hashName(),
                'photo' => $photo,
            ];
            
            $photo = $this->photo_service->create($data);

            return $this->response($this->generateItem($photo), 201);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
