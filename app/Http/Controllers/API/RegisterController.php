<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\UserService;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class RegisterController extends RestController
{
    protected $transformer = UserTransformer::class;

    /**
     * @var UserService
     */
    protected $user_service;

    /**
     * RegisterController construct.
     *
     * @param UserService $user_service
     */
    public function __construct(UserService $user_service)
    {
        parent::__construct();

        $this->user_service = $user_service;
    }

    /**
     * Handle registration.
     *
     * @param Request $request
     * @return void
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        try {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ];

            $user = $this->user_service->create($data);

            return $this->response($this->generateItem($user), 201);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
