<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\ReportService;
use App\Transformers\ReportTransformer;
use Illuminate\Http\Request;

class ReportController extends RestController
{
    protected $transformer = ReportTransformer::class;

    public function store(Request $request, ReportService $report_service)
    {
        $request->validate([
            'name' => 'required',
            'campaignId' => 'required',
            'fileIds' => 'required',
        ]);

        try {
            $data = [
                'name' => $request->name,
                'campaign_id' => $request->campaignId,
                'file_ids' => $request->fileIds,
            ];

            $report = $report_service->create($data);

            return $this->response($this->generateItem($report), 201);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
