<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\SchoolService;
use App\Transformers\SchoolTransformer;

class SchoolController extends RestController
{
    protected $transformer = SchoolTransformer::class;

    /**
     * @var SchoolService
     */
    protected $school_service;

    /**
     * SchoolController construct.
     *
     * @param SchoolService $school_service
     */
    public function __construct(SchoolService $school_service)
    {
        parent::__construct();

        $this->school_service = $school_service;
    }

    /**
     * Handle request to fetch all schools.
     *
     * @return void
     */
    public function get()
    {
        try {
            $schools = $this->school_service->get();

            return $this->response($this->generateCollection($schools));
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
