<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\RestController;
use App\Services\StudentService;
use App\Transformers\StudentTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class StudentController extends RestController
{
    protected $transformer = StudentTransformer::class;

    /**
     * @var StudentServuce
     */
    protected $student_service;

    /**
     * StudentController construct.
     *
     * @param StudentService $student_service
     */
    public function __construct(StudentService $student_service)
    {
        parent::__construct();

        $this->student_service = $student_service;
    }

    /**
     * Find a student handler.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $student = $this->student_service->find($id);

            return $this->response($this->generateItem($student));
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }

    /**
     * Store new student handler.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'schoolId' => 'required',
            'dateOfBirth' => 'required',
            'phoneNumber' => 'required',
        ]);

        try {
            $data = [
                'name' => $request->name,
                'school_id' => $request->schoolId,
                'date_of_birth' => $request->dateOfBirth,
                'phone_number' => $request->phoneNumber,
            ];

            $student = $this->student_service->create($data);

            return $this->response($this->generateItem($student), 201);
        } catch (\Exception $e) {
            return $this->sendIseResponse($e->getMessage());
        }
    }
}
