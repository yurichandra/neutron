<?php

namespace App\Listeners;

use App\DonationStatus;
use App\Events\DonationVerified;
use App\Services\DonationService;
use App\Services\CampaignService;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateDonationStatus
{
    /**
     * @var DonationService
     */
    protected $donation_service;

    /**
     * @var CampaignService
     */
    protected $campaign_service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DonationService $donation_service, CampaignService $campaign_service)
    {
        $this->donation_service = $donation_service;
        $this->campaign_service = $campaign_service;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DonationVerified $event)
    {
        $donation = $event->getDonation();

        $donation_data = [
            'donation_status_id' => DonationStatus::COMPLETED,
            'verify_at' => Carbon::now(),
        ];

        $campaign = $donation->campaign;
        $total = $campaign->donation_received + $donation->amount;

        $campaign_data = [
            'donation_received' => $total,
        ];

        $this->donation_service->update($donation->id, $donation_data);
        $this->campaign_service->update($campaign->id, $campaign_data);
    }
}
