<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'photoable_id',
        'photoable_type',
        'path',
    ];

    /**
     * Get the owning of photoable model.
     *
     * @return void
     */
    public function photoable()
    {
        return $this->morphTo();
    }

    /**
     * Accessor of url attribute.
     *
     * @return void
     */
    public function getUrlAttribute()
    {
        return env('AWS_BUCKET_URL') . "/{$this->path}";
    }
}
