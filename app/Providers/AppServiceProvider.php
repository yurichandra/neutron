<?php

namespace App\Providers;

use App\Services\BankAccountService;
use App\Services\CampaignService;
use App\Services\DonationService;
use App\Services\FileService;
use App\Services\PhotoService;
use App\Services\ReportService;
use App\Services\UniqueCodeService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CampaignService::class, function () {
            return new CampaignService(new PhotoService);
        });

        $this->app->bind(DonationService::class, function () {
            return new DonationService(new BankAccountService, app(CampaignService::class));
        });

        $this->app->bind(ReportService::class, function () {
            return new ReportService(new FileService);
        });

        $this->app->bind(UserService::class, function () {
            return new UserService(
                app(DonationService::class),
                app(CampaignService::class)
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $unique_code = app(UniqueCodeService::class);

        if (! $unique_code->isKeyExist()) {
            $unique_code->init();
        }
    }
}
