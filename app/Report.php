<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    /**
     * Fillable attribute.
     *
     * @return void
     */
    protected $fillable = [
        'campaign_id',
        'name',
    ];

    /**
     * Define belongsTo relation with campaign.
     *
     * @return void
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Define hasMany relation.
     *
     * @return void
     */
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
