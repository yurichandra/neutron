<?php

namespace App\Services;

use App\BankAccount;

class BankAccountService
{
    /**
     * Return all bank account available.
     *
     * @return void
     */
    public function get()
    {
        return BankAccount::with('bank')->get();
    }

    /**
     * Find a bank account.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        return BankAccount::findOrFail($id);
    }

    /**
     * Return all bank accounts by bank ID.
     *
     * @param int $bank_id
     * @return void
     */
    public function getByBank($bank_id)
    {
        return BankAccount::where('bank_id', $bank_id)->get();
    }
}
