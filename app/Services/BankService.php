<?php

namespace App\Services;

use App\Bank;

class BankService
{
    /**
     * Return all banks.
     *
     * @return void
     */
    public function get()
    {
        return Bank::get();
    }
}
