<?php

namespace App\Services;

use App\Campaign;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CampaignService
{
    protected $photo_service;

    /**
     * CampaignService contract.
     *
     * @param PhotoService $photo_service
     */
    public function __construct(PhotoService $photo_service)
    {
        $this->photo_service = $photo_service;
    }

    /**
     * Return all campaigns.
     *
     * @return Collection
     */
    public function get()
    {
        $relation = [
            'student',
            'photos',
            'donations',
        ];

        return Campaign::with($relation)->get();
    }

    /**
     * Return a campaign
     *
     * @param int $id
     * @return Campaign
     */
    public function find($id)
    {
        try {
            return Campaign::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw $e;
        }
    }

    /**
     * Store new campaign.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $campaign = Campaign::create($data);
            $this->attachImages($campaign, $data['photoIds']);

            return $campaign;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Find campaign by user id.
     *
     * @param int $user_id
     * @return void
     */
    public function findByUser($user_id)
    {
        try {
            $campaigns = Campaign::where('user_id', $user_id)->get();

            return $campaigns;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a campaign
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, array $data)
    {
        try {
            $campaign = $this->find($id);
            $campaign->update($data);

            return $campaign;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Attach images to its campaign.
     *
     * @param Campaign $campaign
     * @param array $photo_ids
     * @return void
     */
    public function attachImages(Campaign $campaign, array $photo_ids)
    {
        try {
            collect($photo_ids)->map(function ($item) {
                return $this->photo_service->find($item);
            })
            ->each(function ($item) use ($campaign) {
                $campaign->photos()->save($item);
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
