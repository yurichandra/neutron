<?php

namespace App\Services;

use App\DonationConfirmation;

class DonationConfirmationService
{
    /**
     * Return donation confirmation available.
     *
     * @return void
     */
    public function get()
    {
        return DonationConfirmation::with('donation')->get();
    }

    public function find($id)
    {
        return DonationConfirmation::findOrFail($id);
    }
}
