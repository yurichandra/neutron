<?php

namespace App\Services;

use App\Donation;
use App\DonationConfirmation;
use App\Exceptions\SameUserException;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DonationService
{
    protected $bank_account_service;

    protected $campaign_service;

    /**
     * DonationService construct.
     *
     * @param BankAccountService $bank_account_service
     */
    public function __construct(BankAccountService $bank_account_service, CampaignService $campaign_service)
    {
        $this->bank_account_service = $bank_account_service;
        $this->campaign_service = $campaign_service;
    }

    /**
     * Return all donation with its relation.
     *
     * @return void
     */
    public function get()
    {
        $relation = [
            'status',
            'user',
        ];

        return Donation::with($relation)->get();
    }

    /**
     * Find a donation
     *
     * @param int $id
     * @return Donation
     */
    public function find($id)
    {
        try {
            return Donation::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw $e;
        }
    }

    /**
     * Return all donation by a user ID.
     *
     * @param int $user_id
     * @return void
     */
    public function getMyDonation($user_id)
    {
        try {
            return Donation::where('user_id', $user_id)->get();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create a new donation.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $campaign = $this->campaign_service->find($data['campaign_id']);

            if ($campaign->user_id === $data['user_id']) {
                throw new SameUserException;
            }

            return Donation::create($data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update donation model
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, array $data)
    {
        try {
            $donation = $this->find($id);
            $donation->update($data);

            return $donation->refresh();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Confirm donation.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function confirm($id, array $data)
    {
        try {
            $donation = $this->find($id);
            $bank_account_id = json_decode($donation->meta, true)['bank_account_id'];
            $bank = $this->bank_account_service->find($bank_account_id)->bank;

            $data = [
                'donation_id' => (int) $id,
                'bank_id' => $bank->id,
                'account_holder' => $data['account_holder'],
                'amount_to_paid' => $data['amount'],
                'date' => $data['date'],
            ];

            return DonationConfirmation::create($data);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
