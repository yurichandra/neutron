<?php

namespace App\Services;

use App\File;
use Illuminate\Support\Facades\Storage;

class FileService
{
    /**
     * Find a file by ID.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            return File::findOrFail($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Store a new file.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $this->upload($data);

            return File::create($data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Uploading file to external services.
     *
     * @param array $data
     * @return void
     */
    public function upload(array $data)
    {
        try {
            Storage::disk('s3')->put($data['name'], file_get_contents($data['file']), 'public');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
