<?php

namespace App\Services;

use App\Photo;
use Illuminate\Support\Facades\Storage;

class PhotoService
{
    /**
     * Find a photo by ID.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            return Photo::findOrFail($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Store a new photo.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $this->upload($data);

            return Photo::create([
                'photoable_id' => 0,
                'photoable_type' => null,
                'path' => $data['path'],
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Uploading photo to external services.
     *
     * @param array $data
     * @return void
     */
    public function upload(array $data)
    {
        try {
            Storage::disk('s3')->put($data['path'], file_get_contents($data['photo']), 'public');
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
