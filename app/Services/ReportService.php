<?php

namespace App\Services;

use App\Report;

class ReportService
{
    protected $file_service;

    public function __construct(FileService $file_service)
    {
        $this->file_service = $file_service;
    }

    /**
     * Create a new report.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $report = Report::create($data);
            $this->attachFiles($report, $data['file_ids']);

            return $report;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Attaching files to a report.
     *
     * @param Report $report
     * @param array $photo_ids
     * @return void
     */
    public function attachFiles(Report $report, array $file_ids)
    {
        try {
            collect($file_ids)->map(function ($item) {
                return $this->file_service->find($item);
            })
            ->each(function ($item) use ($report) {
                $report->files()->save($item);
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
