<?php

namespace App\Services;

use App\School;

class SchoolService
{
    /**
     * Return all available school.
     *
     * @return void
     */
    public function get()
    {
        return School::get();
    }
}
