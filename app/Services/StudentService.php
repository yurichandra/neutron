<?php

namespace App\Services;

use App\Student;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StudentService
{
    /**
     * Return all students.
     *
     * @return void
     */
    public function get()
    {
        return Student::with('schools')->get();
    }

    /**
     * Return a student.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            return Student::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw $e;
        }
    }

    /**
     * Create a new student.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            return Student::create($data);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
