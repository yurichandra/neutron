<?php

namespace App\Services;

use Illuminate\Support\Facades\Redis;

class UniqueCodeService
{
    const KEY = "unique_code";

    /**
     * Init unique code available.
     *
     * @param integer $start
     * @param integer $end
     * @return void
     */
    public function init($start = 250, $end = 500)
    {
        Redis::pipeline(function ($pipe) use ($start, $end) {
            for ($i = $start; $i <= $end; $i++) {
                $pipe->rpush(self::KEY, $i);
            };
        });
    }

    /**
     * Generate return an unique code.
     *
     * @return void
     */
    public function generate()
    {
        return Redis::lpop(self::KEY);
    }

    /**
     * Put unique code back again in cache.
     *
     * @param int $unique_code
     * @return void
     */
    public function put($unique_code)
    {
        return Redis::rpush(self::key, $unique_code);
    }

    /**
     * Check whether key exist or not.
     *
     * @return boolean
     */
    public function isKeyExist()
    {
        return Redis::exists(self::KEY);
    }
}
