<?php

namespace App\Services;

use App\User;

class UserService
{
    /**
     * @var DonationService
     */
    protected $donation_service;

    /**
     * @var CampaignService
     */
    protected $campaign_service;

    /**
     * UserService construct.
     *
     * @param DonationService $donation_service
     * @param CampaignService $campaign_service
     */
    public function __construct(DonationService $donation_service, CampaignService $campaign_service)
    {
        $this->donation_service = $donation_service;
        $this->campaign_service = $campaign_service;
    }

    /**
     * Store new user.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        return User::create($data);
    }

    /**
     * Get all my activites data.
     *
     * @param int $user_id
     * @return void
     */
    public function myActivity($user_id)
    {
        try {
            $my_donation = $this->donation_service->getMyDonation($user_id);
            $my_campaign = $this->campaign_service->findByUser($user_id);

            $donation_amount_total = collect($my_donation)->reduce(function ($carry, $item) {
                $carry += $item->amount;

                return $carry;
            }, 0);
            
            $campaign_amount_total = collect($my_campaign)->reduce(function ($carry, $item) {
                $carry += $item->donation_received;

                return $carry;
            }, 0);

            $campaign_total = count($my_campaign);

            return [
                'donation_amount_total' => $donation_amount_total,
                'campaign_amount_total' => $campaign_amount_total,
                'campaign_total' => $campaign_total,
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
