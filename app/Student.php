<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'school_id',
        'date_of_birth',
        'phone_number',
    ];

    /**
     * Define belongsTo relation.
     *
     * @return void
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Define hasMany relation with Campaign model.
     *
     * @return void
     */
    public function campaign()
    {
        return $this->hasMany(Campaign::class);
    }
}
