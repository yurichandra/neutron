<?php

namespace App\Transformers;

use App\BankAccount;
use League\Fractal\TransformerAbstract;

class BankAccountTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'bank',
    ];

    /**
     * Transform BankAccount model on response.
     *
     * @param BankAccount $bank_account
     * @return void
     */
    public function transform(BankAccount $bank_account)
    {
        return [
            'id' => $bank_account->id,
            'name' => $bank_account->name,
            'accountNumber' => $bank_account->account_number,
        ];
    }

    /**
     * Include bank model on transform.
     *
     * @param BankAccount $bank_account
     * @return void
     */
    public function includeBank(BankAccount $bank_account)
    {
        return $this->item($bank_account->bank, new BankTransformer);
    }
}
