<?php

namespace App\Transformers;

use App\Bank;
use League\Fractal\TransformerAbstract;

class BankTransformer extends TransformerAbstract
{
    /**
     * Transform bank model on response.
     *
     * @param Bank $bank
     * @return void
     */
    public function transform(Bank $bank)
    {
        return [
            'id' => $bank->id,
            'name' => $bank->name,
        ];
    }
}
