<?php

namespace App\Transformers;

use App\Campaign;
use League\Fractal\TransformerAbstract;

class CampaignTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'campaigner',
        'student',
        'photos',
        'donations',
        'reports',
    ];

    /**
     * Transform Campaign model on response.
     *
     * @param Campaign $campaign
     * @return void
     */
    public function transform(Campaign $campaign)
    {
        return [
            'id' => $campaign->id,
            'title' => $campaign->title,
            'date' => $campaign->date,
            'description' => $campaign->description,
            'donationTarget' => $campaign->donation_target,
            'donationReceived' => $campaign->donation_received,
        ];
    }

    /**
     * Include user/campaigner model on transform.
     *
     * @param Campaign $campaign
     * @return void
     */
    public function includeCampaigner(Campaign $campaign)
    {
        return $this->item($campaign->user, new UserTransformer);
    }

    /**
     * Include student model on transform.
     *
     * @param Campaign $campaign
     * @return void
     */
    public function includeStudent(Campaign $campaign)
    {
        return $this->item($campaign->student, new StudentTransformer);
    }

    /**
     * Include photo model on transform.
     *
     * @param Campaign $campaign
     * @return void
     */
    public function includePhotos(Campaign $campaign)
    {
        return $this->collection($campaign->photos, new PhotoTransformer);
    }

    /**
     * Including donations on transform.
     *
     * @param Campaign $campaign
     * @return void
     */
    public function includeDonations(Campaign $campaign)
    {
        return $this->collection($campaign->donations, new DonationTransformer);
    }

    /**
     * Including report on transform.
     *
     * @param Campaign $campaign
     * @return void
     */
    public function includeReports(Campaign $campaign)
    {
        return $this->collection($campaign->reports, new ReportTransformer);
    }
}
