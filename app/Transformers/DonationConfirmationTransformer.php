<?php

namespace App\Transformers;

use App\DonationConfirmation;
use League\Fractal\TransformerAbstract;

class DonationConfirmationTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['donation'];

    /**
     * Transform model.
     *
     * @param DonationConfirmation $donation_confirmation
     * @return void
     */
    public function transform(DonationConfirmation $donation_confirmation)
    {
        return [
            'id' => $donation_confirmation->id,
            'accountHolder' => $donation_confirmation->account_holder,
            'amount' => $donation_confirmation->amount_to_paid,
            'date' => $donation_confirmation->date,
        ];
    }

    /**
     * Include donation on transform.
     *
     * @param DonationConfirmation $donation_confirmation
     * @return void
     */
    public function includeDonation(DonationConfirmation $donation_confirmation)
    {
        return $this->item($donation_confirmation->donation, new DonationTransformer);
    }
}
