<?php

namespace App\Transformers;

use App\DonationStatus;
use League\Fractal\TransformerAbstract;

class DonationStatusTransformer extends TransformerAbstract
{
    /**
     * Transform donation status model.
     *
     * @param DonationStatus $donation_status
     * @return void
     */
    public function transform(DonationStatus $donation_status)
    {
        return [
            'id' => $donation_status->id,
            'name' => $donation_status->name,
        ];
    }
}
