<?php

namespace App\Transformers;

use App\Donation;
use League\Fractal\TransformerAbstract;

class DonationTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'donors',
        'status',
    ];

    public function transform(Donation $donation)
    {
        return [
            'id' => $donation->id,
            'amount' => $donation->amount,
            'occuredAt' => $donation->occured_at,
            'meta' => json_decode($donation->meta),
        ];
    }

    /**
     * Including donors (user) on transform.
     *
     * @param Donation $donation
     * @return void
     */
    public function includeDonors(Donation $donation)
    {
        return $this->item($donation->user, new UserTransformer);
    }

    /**
     * Including donation status on transform.
     *
     * @param Donation $donation
     * @return void
     */
    public function includeStatus(Donation $donation)
    {
        return $this->item($donation->status, new DonationStatusTransformer);
    }
}
