<?php

namespace App\Transformers;

use App\File;
use League\Fractal\TransformerAbstract;

class FileTransformer extends TransformerAbstract
{
    /**
     * Transform File model on response.
     *
     * @param File $File
     * @return void
     */
    public function transform(File $file)
    {
        return [
            'id' => $file->id,
            'type' => $file->type,
            'url' => $file->url,
        ];
    }
}
