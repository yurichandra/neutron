<?php

namespace App\Transformers;

use App\Photo;
use League\Fractal\TransformerAbstract;

class PhotoTransformer extends TransformerAbstract
{
    /**
     * Transform photo model on response.
     *
     * @param Photo $photo
     * @return void
     */
    public function transform(Photo $photo)
    {
        return [
            'id' => $photo->id,
            'url' => $photo->url,
        ];
    }
}
