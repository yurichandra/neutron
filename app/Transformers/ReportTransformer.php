<?php

namespace App\Transformers;

use App\Report;
use League\Fractal\TransformerAbstract;

class ReportTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'files',
    ];

    /**
     * Transform report on response.
     *
     * @param Report $report
     * @return void
     */
    public function transform(Report $report)
    {
        return [
            'id' => $report->id,
            'name' => $report->name,
        ];
    }

    /**
     * Include files on transform.
     *
     * @param Report $report
     * @return void
     */
    public function includeFiles(Report $report)
    {
        return $this->collection($report->files, new FileTransformer);
    }
}
