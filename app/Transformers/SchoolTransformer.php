<?php

namespace App\Transformers;

use App\School;
use League\Fractal\TransformerAbstract;

class SchoolTransformer extends TransformerAbstract
{
    /**
     * Transform School model on response.
     *
     * @param School $school
     * @return void
     */
    public function transform(School $school)
    {
        return [
            'id' => $school->id,
            'name' => $school->name,
            'address' => $school->address,
            'phoneNumber' => $school->phone_number,
        ];
    }
}
