<?php

namespace App\Transformers;

use App\Student;
use League\Fractal\TransformerAbstract;

class StudentTransformer extends TransformerAbstract
{
    /**
     * Available includes of this model.
     */
    protected $defaultIncludes = [
        'school',
    ];

    /**
     * Transform Student model on response.
     *
     * @param Student $student
     * @return void
     */
    public function transform(Student $student)
    {
        return [
            'id' => $student->id,
            'name' => $student->name,
            'dateOfBirth' => $student->date_of_birth,
        ];
    }

    /**
     * Include school model transformer.
     *
     * @param Student $student
     * @return void
     */
    public function includeSchool(Student $student)
    {
        return $this->item($student->school, new SchoolTransformer);
    }
}
