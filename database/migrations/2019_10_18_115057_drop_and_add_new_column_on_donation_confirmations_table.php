<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropAndAddNewColumnOnDonationConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donation_confirmations', function (Blueprint $table) {
            $table->dropColumn('account_number');
            $table->unsignedBigInteger('bank_id');

            $table->foreign('bank_id')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donation_confirmations', function (Blueprint $table) {
            $table->string('account_number');
            $table->dropForeign('donation_confirmations_bank_id_foreign');
            $table->dropColumn('bank_id');
        });
    }
}
