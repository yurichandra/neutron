<?php

use App\BankAccount;
use Illuminate\Database\Seeder;

class BankAccountsTableSeeder extends Seeder
{
    protected $data = [
        [
            'id' => 1,
            'bank_id' => 1,
            'name' => 'Ayo Sekolah',
            'account_number' => '987654321',
        ],
        [
            'id' => 2,
            'bank_id' => 2,
            'name' => 'Ayo Sekolah',
            'account_number' => '5678901234',
        ],
        [
            'id' => 3,
            'bank_id' => 3,
            'name' => 'Ayo Sekolah',
            'account_number' => '3456789012',
        ],
        [
            'id' => 4,
            'bank_id' => 4,
            'name' => 'Ayo Sekolah',
            'account_number' => '123456789',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            if (is_null(BankAccount::find($item['id']))) {
                BankAccount::create($item);
            }
        }
    }
}
