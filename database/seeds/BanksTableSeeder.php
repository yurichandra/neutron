<?php

use App\Bank;
use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    protected $data = [
        [
            'id' => 1,
            'name' => 'Bank Mandiri',
        ],
        [
            'id' => 2,
            'name' => 'Bank BCA',
        ],
        [
            'id' => 3,
            'name' => 'Bank BRI',
        ],
        [
            'id' => 4,
            'name' => 'Bank Permata',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            if (is_null(Bank::find($item['id']))) {
                Bank::create($item);
            }
        }
    }
}
