<?php

use App\DonationStatus;
use Illuminate\Database\Seeder;

class DonationStatusesTableSeeder extends Seeder
{
    protected $data = [
        [
            'id' => 1,
            'name' => 'Pending',
        ],
        [
            'id' => 2,
            'name' => 'Waiting to be Verified',
        ],
        [
            'id' => 3,
            'name' => 'Completed',
        ],
        [
            'id' => 4,
            'name' => 'Waiting for Payment',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            if (is_null(DonationStatus::find($item['id']))) {
                DonationStatus::create($item);
            }
        }
    }
}
