<?php

use App\School;
use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
    protected $data = [
        [
            'id' => 1,
            'name' => 'SMA 10 Yogyakarta',
            'address' => 'Jl. Gadean No.5, Ngupasan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122',
            'phone_number' => '0274562458',
        ],
        [
            'id' => 2,
            'name' => 'SMA Bopkri 1 Yogyakarta',
            'address' => 'Jl. Wardhani No.2, Kotabaru, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224',
            'phone_number' => '0274515359',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            if (is_null(School::find($item['id']))) {
                School::create($item);
            }
        }
    }
}
