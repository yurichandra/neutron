<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    protected $data = [
        [
            'id' => 1,
            'name' => 'Yuri Chandra',
            'email' => 'yurichandra@gmail.com',
            'password' => 'Ayosekolah123',
        ],
        [
            'id' => 2,
            'name' => 'Yafet Trakan',
            'email' => 'yafet.trakan@gmail.com',
            'password' => 'Ayosekolah123',
        ],
        [
            'id' => 3,
            'name' => 'Agung Prio',
            'email' => 'agungskak22@gmail.com',
            'password' => 'Ayosekolah123',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            if (is_null(User::find($item['id']))) {
                User::create([
                    'name' => $item['name'],
                    'email' => $item['email'],
                    'password' => bcrypt($item['password']),
                ]);
            }
        }
    }
}
