import Vue from 'vue'

import vuetify from './plugins/vuetify'

import router from './router'

import store from './store'

import Admin from './views/Admin'

new Vue({
  el: '#app',
  vuetify,
  router,
  store,
  render: h => h(Admin),

  created() {
    try {
      auth.refresh()
    } catch (err) {
      // Do nothing :))
    }
  }
})
  .$mount('#app');