import Vue from 'vue'
import VueRouter from 'vue-router'
import cookie from 'js-cookie'

import store from '../store'

import LoginView from '../views/Login'
import DashboardView from '../views/Dashboard'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      name: 'dashboard',
      path: '/admin',
      component: DashboardView,
      beforeEnter: (to, from, next) => {
        const session = cookie.get('accessToken')

        if (!session) {
          next('/admin/login')
        }

        store.commit('authenticated')
        next()
      }
    },
    {
      name: 'login',
      path: '/admin/login',
      component: LoginView
    }
  ]
})