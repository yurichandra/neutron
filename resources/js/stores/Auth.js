import cookie from 'js-cookie'

const state = {
  isAuthenticated: false,
}

const getters = {
  isAuthenticated: state => state.isAuthenticated
}

const mutations = {
  authenticateSuccess (state, token) {
    state.isAuthenticated = true
    cookie.set('accessToken', token)
  },

  authenticated (state) {
    state.isAuthenticated = true
  },

  logout (state) {
    cookie.remove('accessToken')
    state.isAuthenticated = false
  }
}

export default {
  state,
  getters,
  mutations
}