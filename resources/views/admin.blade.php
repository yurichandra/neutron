<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <title>Ayo Sekolah | Admin Page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <style>
    body {
      font-family: 'Roboto', sans-serif;
    }

    .b-4 {
      border-radius: 4px;
    }
  </style>
</head>
<body>
  <div id="app">

  </div>

  <script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>