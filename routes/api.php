<?php

$config = [
    'namespace' => 'API',
    'middleware' => 'auth:api',
];

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'IndexController@index');

Route::post('/register', 'API\RegisterController@register');

Route::group($config, function () {
    Route::get('whoami', 'AuthController@whoami');
    Route::get('my/activites', 'MyController@getMyActivity');

    Route::get('schools', 'SchoolController@get');

    Route::post('students', 'StudentController@store');
    Route::get('students/{id}', 'StudentController@find');

    Route::post('campaigns', 'CampaignController@store');
    Route::get('campaigns', 'CampaignController@get');
    Route::get('campaigns/{id}', 'CampaignController@find');
    Route::get('my/campaigns', 'CampaignController@getMyCampaign');

    Route::get('donations', 'DonationController@get');
    Route::post('donations', 'DonationController@store');
    Route::get('donations/{id}', 'DonationController@find');
    Route::post('donations/{id}/confirm', 'DonationController@confirm');
    Route::post('donations/{id}/verify', 'DonationController@verify');
    Route::get('my/donations', 'DonationController@getMyDonation');

    Route::get('donation-confirms', 'DonationConfirmationController@get');

    Route::post('reports', 'ReportController@store');

    Route::post('photos', 'PhotoController@store');
    Route::post('files', 'FileController@store');

    Route::get('banks', 'BankController@get');

    Route::get('bank-accounts/{bankId}', 'BankAccountController@getByBank');
});
